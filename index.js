var arrBai1 = []; //tạo mảng trống phía bên ngoài function để lưu trữ

//bài 1: Tính tổng số dương tỉng mảng
function tongSoDuong() {
  var tongSoDuongTrongMang = 0;

  var soDuongNhoNhat = arrBai1[0];

  for (var i = 1; i < arrBai1.length; i++) {
    if (soDuongNhoNhat > arrBai1[i]) {
      soDuongNhoNhat = arrBai1[i];
    }
  }

  for (var i = 0; i < arrBai1.length; i++) {
    var arr = arrBai1[i];

    if (arr > 0) {
      tongSoDuongTrongMang = tongSoDuongTrongMang + arr;
    }
  }

  return tongSoDuongTrongMang;
}
//bài 2: Đếm số dương trong mảng
function demSoDuong() {
  var demSoDuong = 0;

  arrBai1.forEach(function (item) {
    if (item > 0) {
      demSoDuong++;
    }
  });
  return demSoDuong;
}
//bài 3: Tìm số nhỏ nhất trong mảng
function timSoNhoNhatTrongMang() {
  // var min = Math.min(...arrBai1);
  var smallest = arrBai1[0];
  for (var i = 1; i < arrBai1.length; i++) {
    var arr4 = arrBai1[i];

    if (arr4 < smallest) {
      smallest = arr4;
    }
  }
  return smallest;
}
//bài 4: Tìm số dương nhỏ nhất trong mảng
function soDuongNhoNhat() {
  let smallestPosativeNumber = arrBai1.find((e) => {
    return e > 0;
  });
  for (var i = 0; i < arrBai1.length; i++) {
    var arr4 = arrBai1[i];
    if (arr4 < 0) {
      continue;
    }
    if (arr4 < smallestPosativeNumber) {
      smallestPosativeNumber = arr4;
    }
  }

  return smallestPosativeNumber;
}
//bài 5: tìm số chẵn cuối cùng
function timSoChanCuoiCung() {
  var soChanCuoiCung = "không có";

  for (var i = 0; i < arrBai1.length; i++) {
    if (arrBai1[i] % 2 === 0) {
      soChanCuoiCung = arrBai1[i];
    }
  }
  return soChanCuoiCung;
}
//bài 6: Đổi vị trí 2 số trong mảng
function swapValues() {
  var index1 = document.getElementById("vi-tri-1").value * 1;
  var index2 = document.getElementById("vi-tri-2").value * 1;

  return hoanDoiArray(arrBai1, index1, index2);
}

function hoanDoiArray(array, index1, index2) {
  const arrCopy = [...array];
  [arrCopy[index2], arrCopy[index1]] = [arrCopy[index1], arrCopy[index2]];
  return arrCopy;
}

//push giá trị người dùng nhập vào mảng
function pushArr() {
  var nhapSo = document.getElementById("tong-so-duong");
  var nhapSoEl = nhapSo.value * 1;

  arrBai1.push(nhapSoEl); // push thêm giá trị vào mảng
  nhapSo.value = "";
}
//render
function render(
  arrBai1,
  tongSoDuongTrongMang,
  soDuong,
  smallest,
  smallestPosativeNumber,
  soChanCuoiCung,
  arrDaDoiViTri,
  sapXepTangDan,
  soNguyenToDauTien,
  hamDemSoNguyen,
  soSanhAmDuong
) {
  var contentHTML = `<h4>arrBai1= [${arrBai1}]</h4>
  <h4>Tổng số dương trong mảng: ${tongSoDuongTrongMang}</h4>
  <h4>Số dương trong mảng: ${soDuong}</h4>
  <h4>Số nhỏ nhất trong mảng: ${smallest}</h4>
  <h4>Số dương nhỏ nhất trong mảng: ${smallestPosativeNumber}</h4>
  <h4>Số chẵn cuối cùng trong mảng: ${soChanCuoiCung}</h4>
  <h4>Mảng đã thay đổi vị trí: ${arrDaDoiViTri}</h4>
  <h4>Mảng đã sắp xếp từ nhỏ tới lớn: ${sapXepTangDan}</h4>
  <h4>số Nguyên tố đầu tiên trong mảng: ${soNguyenToDauTien}</h4>
  <h4>số lượng Số Nguyên trong mảng: ${hamDemSoNguyen}</h4>
 
  <h4>Âm và Dương số nào nhiều hơn: ${soSanhAmDuong}</h4>
  `;

  document.getElementById("result-tong-so-duong").innerHTML = contentHTML;
}

function sapXepMangTangDan() {
  const arrCopy = [...arrBai1];
  arrCopy.sort((a, b) => a - b); // a-b : tăng dần ; b-a : giảm dần
  return arrCopy;
}

function timSoNguyenToDauTienTrongMang(number) {
  var contentSoNguyenToDauTien = arrBai1.find(function (item, index, array) {
    // console.log(kiemTraSoNguyenTo(item));
    //     return kiemTraSoNguyenTo(item);

    var kiemtranNT = kiemTraSoNguyenTo(item);
    if (kiemtranNT === true) {
      console.log(item, "la số nguyên to");
      return true;
    }
    if (kiemtranNT === false) {
      console.log(item, " không phai số nguyên tố");
      return false;
    }
  });
  console.log(contentSoNguyenToDauTien);
  return contentSoNguyenToDauTien;
  // kiemTraSoNguyenTo(...);
}

function kiemTraSoNguyenTo(number) {
  if (number < 2) return false;

  var canBac2 = Math.sqrt(number);

  for (var i = 2; i <= canBac2; i++) {
    if (number % i == 0) {
      return false;
    }
  }
  return true;
}

function demSoNguyen() {
  var countSoNguyen = 0;
  arrBai1.forEach(function (item) {
    if (Number.isInteger(item) === true) {
      countSoNguyen++;
    }
  });
  console.log(countSoNguyen);
  return countSoNguyen;
}

function demSoDuong() {
  var countSoDuong = 0;
  arrBai1.forEach(function (item) {
    if (item > 0) {
      countSoDuong++;
    }
  });
  return countSoDuong;
}
function demSoAm() {
  var countSoAm = 0;
  arrBai1.forEach(function (item) {
    if (item < 0) {
      countSoAm++;
    }
  });
  return countSoAm;
}

function soSanhSoDuongVaSoAm() {
  var soDuong = demSoDuong();
  var soAm = demSoAm();
  if (soDuong > soAm) {
    return "Số Dương > số Âm";
  }
  if (soAm > soDuong) {
    return "Số Âm > số Dương";
  }
  if (soAm === soDuong){
    return "Số Âm = số Dương ";
  }

}

function nhapNutTinhToan() {
  pushArr();
  var tongSoDuongTrongMang = tongSoDuong();
  var soDuong = demSoDuong();
  var smallest = timSoNhoNhatTrongMang();
  var smallestPosativeNumber = soDuongNhoNhat();
  var soChanCuoiCung = timSoChanCuoiCung();
  var arrDaDoiViTri = swapValues();
  var sapXepTangDan = sapXepMangTangDan();
  var soNguyenToDauTien = timSoNguyenToDauTienTrongMang();
  var hamDemSoNguyen = demSoNguyen();
  var soSanhAmDuong = soSanhSoDuongVaSoAm();
  //render
  render(
    arrBai1,
    tongSoDuongTrongMang,
    soDuong,
    smallest,
    smallestPosativeNumber,
    soChanCuoiCung,
    arrDaDoiViTri,
    sapXepTangDan,
    soNguyenToDauTien,
    hamDemSoNguyen,
    soSanhAmDuong
  );
}
